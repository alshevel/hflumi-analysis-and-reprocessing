import matplotlib.pyplot as plt
import argparse, yaml

from pybrildpg import hd5_reader
from pybrildpg import plotter

# Configure detector
parser = argparse.ArgumentParser(description="The tool is designed to analyse CMS BRIL data and generate DPG relevant plots and comparisons")

parser.add_argument('detector1', type=str, help='First detector in comparison')
parser.add_argument('detector2', type=str, help='Second detector in comparison')
parser.add_argument('fill', type=int, help='Fill to check')
parser.add_argument('config', type=str, help='Configuration file with detector meta data')

args = parser.parse_args()

config_file = args.config
fill = args.fill

stable_fills = [fill]

with open(config_file, 'r') as file:
    meta_data = yaml.safe_load(file)

beam = meta_data['beam']
first_d = meta_data[args.detector1]
second_d = meta_data[args.detector2]

for fill in stable_fills:
    try:

        beam = hd5_reader.read_custom_fill(beam['path'], fill, beam['node'])
    
        data1 = hd5_reader.read_custom_fill(first_d['path'], fill, first_d['node'])
        data2 = hd5_reader.read_custom_fill(second_d['path'], fill, second_d['node'])

        plotter.sbil_ratio( beam
                          , data1, data2
                          , first_d, second_d
                          , fill
                          )
        '''        
        plotter.ratio_plot( data1, data2
                          , first_d, second_d
                          , fill
                          )
        '''
 
    except:
        print(f'{fill} failed')

plt.show()