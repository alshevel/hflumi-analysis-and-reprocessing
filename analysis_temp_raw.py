import pandas
import numpy as np
import matplotlib.pyplot as plt
import glob

# custom modules
from pybrildpg import hf_correction
from pybrildpg import plotter
from pybrildpg import hd5_reader

# ====================================================================
# Main script to extract corrections and generate plots
# for per fill data
# ====================================================================
beam_path = '/brildata/24/'
file_path = '/brildata/24/'
#custom_file_path = '/brildata/hf_reprocess/24/'

node = 'hfetlumi'

stable_fills = [int(x.replace(file_path, '')) for x in glob.iglob(f'{file_path}/*', recursive = True)]
stable_fills.sort()

print(stable_fills)

# Find 
stable_fills = [10342] # 9003 - single 9004, 9008

for fill in stable_fills:

    #try:
        
        # Read beam data
        beam = hd5_reader.read_beam_fill(beam_path, fill, 'beam')
        beam_slice = beam[['fillnum', 'runnum', 'lsnum', 'nbnum', 'status', 'collidable']]
        
        # TODO: replace all this part to generate per Fill masks and number of colliding
        # Make sure that the first entry is correspond to the given fill
        beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'STABLE BEAMS')]
        #beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'ADJUST')]
        #beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'FLAT TOP')]
        
        beam_loc = beam_loc.loc[(beam_loc['fillnum'] == fill)]
        activeBXMask = np.array(beam_loc['collidable'].iloc[0])
        number_of_active = beam_loc['ncollidable'].iloc[0]

        # BEAMS
        print(activeBXMask)
        print(number_of_active)
        print(beam.columns)
        print(beam['machinemode'].unique())
        print(beam['status'].unique())

        # Read lumi data
        lumi = hd5_reader.read_fill(file_path, fill, node)

        df = pandas.merge( lumi
                         , beam_slice
                         , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])
        
        # Read pedestals
        pedestal = hd5_reader.read_fill(file_path, fill, 'hfEtPedestal')
        pedestal = pedestal.rename(columns = {'data':'ped'})

        df = pandas.merge( df
                         , pedestal
                         , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])

        hf_correction.revert_fixed_pedestal(df)

        # Read afterglow
        afterglow = hd5_reader.read_fill(file_path, fill, 'hfafterglowfrac')
        afterglow = afterglow.rename(columns = {'data':'afterglow'})

        df = pandas.merge( df
                         , afterglow
                         , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])

        df.bxraw = df.bxraw/df.afterglow

        df_flat_top = df.loc[(df['status'].str.decode("utf-8") == 'FLAT TOP')]
        hf_correction.subtract_flat_top_pedestal(df, df_flat_top)

        # For PbPb
        type1 = [0, 0, 0, 0]
        order1 = [0, 0, 0, 0]

        hf_correction.apply_afterglow(df, activeBXMask, type1, order1, path = 'input/afterglow/hfet24.txt')
        #hf_correction.subtract_pedestal(df)

        plotter.plot_hist_bx( np.stack(df.bxraw).mean(axis = 0)
                            , 'without corrections'
                            , fill
                            , False )

        #print(lumi)

        '''

        #df = df.loc[(df['status'].str.decode("utf-8") == 'STABLE BEAMS')]
        #df = df[10000:15000]

        # Useful to have that for some algos
        df['sbil'] = np.array([np.multiply(hist, activeBXMask).sum()/number_of_active for hist in np.stack(df['bxraw'])]).tolist()
        df = df.round({'sbil': 5})
        df = df.sort_values(by=['sbil'])

        #df_flat_top = df.loc[(df['status'].str.decode("utf-8") == 'FLAT TOP')]
        #hf_correction.subtract_flat_top_pedestal(df, df_flat_top)

        #hf_correction.revert_pedestal(df)
        #hf_correction.revert_afterglow(df, activeBXMask, 'input/afterglow/hfoc23.txt')


        # Start main procedures and test all plotters
        #type1 = [0.00625, 0.000577, 0.00048, 7.45e-5]
        type1 = [0, 0, 0, 0]
        order1 = [0, 0, 0, 0]

        # For PbPb
        #hf_correction.apply_afterglow(df, activeBXMask, type1, order1, path = 'input/afterglow/hfet24.txt')
        #hf_correction.subtract_pedestal(df)

        plotter.plot_hist_bx( np.stack(df.bxraw).mean(axis = 0)
                            , 'without corrections'
                            , fill
                            , False )
        
        #plotter.plot_pile_dist(df, 'HFET', fill)

        
        plotter.calculate_residual( df
                                  , activeBXMask
                                  , fill
                                  , node
                                  , '/localdata/alshevel/hf_studies/'
                                  , True # save figure
                                  , True # save hd5 file
                                  )
        
        #plotter.plot_pedestal(df, fill)
        
        
        for type in range(4):
            a, b, c = plotter.calculate_quad_type1( df 
                                                    , activeBXMask
                                                    , type+1 # Type1 j++
                                                    , 1 # Quadratic fit
                                                    , False)
            print(type, a, b, c)
        
        plotter.plot_type2(df, activeBXMask, number_of_active)
        #plotter.plot_instant(df, activeBXMask)
        '''

    #except:
    #    print(f'Fill {fill} has failed')

plt.show()