import pandas
import numpy as np
import matplotlib.pyplot as plt
import glob, pathlib, os

# custom modules
from pybrildpg import hf_correction
from pybrildpg import hd5_reader

# ====================================================================
# Main template for HF reprocessing. TODO: Should be made configurable
# ====================================================================
input_path = '/brildata/24/' #/brildpg/rawdata/17/
custom_file_path = '/brildata/hf_reprocess/24/'
output_path = '/localdata/alshevel/hf_origin/hfet/24'

node = 'hfetlumi'
sigvis = 3333.96

stable_fills = [int(x.replace(input_path, '')) for x in glob.iglob(f'{input_path}/*', recursive = True)]
stable_fills.sort()

print(stable_fills)

stable_fills = [9487] # 9487 single bunch

list_of_failed = []
for fill in stable_fills:
    
    try:
        # Final destination where to save
        fill_path = f'{output_path}/{fill}/'
        if not os.path.isdir(fill_path):
            os.makedirs(fill_path)

        print(fill)

        # Read afterglow and pedestals
        pedestal = hd5_reader.read_fill(custom_file_path, fill, 'hfEtPedestal')
        pedestal = pedestal.rename(columns = {'data':'ped'})

        afterglow = hd5_reader.read_fill(custom_file_path, fill, 'hfafterglowfrac')
        afterglow = afterglow.rename(columns = {'data':'afterglow'})
        

        # Start with run reprocessing   
        for path in pathlib.Path(f'{input_path}{fill}/').glob('*.hd5'):

            df_run = hd5_reader.read_run(path, node)

            file_name = str(path).replace(f'{input_path}{fill}/', '')

            df_run = pandas.merge( df_run
                                 , pedestal
                                 , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])
            
            hf_correction.revert_fixed_pedestal(df_run)

            df_run = pandas.merge( df_run
                                 , afterglow
                                 , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])

            df_run.bxraw = df_run.bxraw/df_run.afterglow
            
            df_run['bx'] = df_run['bxraw'] * 11245.6/sigvis # new calibrations applied to a raw rate
            df_run['avg'] = df_run['avgraw'] * 11245.6/sigvis

            print(df_run)

            hd5_reader.save_to_hd5(df_run, node, fill_path, file_name)
    
    except:
        list_of_failed.append(fill)

print(list_of_failed)