#include <stdio.h>
#include <stdbool.h>

// Module is in the best possible shape

void subtract_type1( int * activeBXMask, float * muHistPerBX, int mod4, float * p0, float  * p1 ) {
    
    // p0 and p1 is array. We expect corrections for 3 folling bunches to match
    // QIE pipeline structure
    
    int ibx, type;
    float frac;
    int type_len = 4;
    int bx_len = 3564;
    
    for(ibx=0; ibx < bx_len - 1; ibx++) {
        if (activeBXMask[ibx] == 1 && ibx % 4 == mod4) {
            for (type=0; type < type_len; type++) {
                frac = p0[type] + p1[type] * muHistPerBX[ibx];
                //printf("type : %i , md : %i, frac: %.8f /n", type, md, frac);
                muHistPerBX[ibx+type] = muHistPerBX[ibx+type] - frac;
            }
        }
    }
}

// For the firs bunch quadratic type1 should be subtracted
void subtract_quadratic( int * activeBXMask, float * muHistPerBX, int type, float * p0, float * p1, float * p2) {
    
    // p0 and p1 is array. We expect corrections for 3 folling bunches to match
    // QIE pipeline structure
    int ibx;
    float frac;
    int bx_len = 3564;
    int mod4;
    
    for(ibx=0; ibx < bx_len - 1; ibx++) {
        if (activeBXMask[ibx] == 1) {
            mod4 = ibx % 4;
            frac = p0[mod4] * (muHistPerBX[ibx] * muHistPerBX[ibx]) +  p1[mod4] * muHistPerBX[ibx] + p2[mod4];
            //printf("type : %i , md : %i, frac: %.8f /n", type, md, frac);
            muHistPerBX[ibx+type] = muHistPerBX[ibx+type] - frac;
        }
    }
}

// Simply C code to reprocess afterglow
void calculate_afterglow( int * activeBXMask, float * muHistPerBX, float * HFSBR, float * type1, float * order1) {

    // Calculate
    int ibx, jbx;
    int bx_len = 3564;
    
    // Default correction
    float cor = 0;
    float quadratic = 0;
    int type;
    int mod4;
    
    for(ibx=0; ibx < bx_len; ibx++) {

        if (activeBXMask[ibx] == 1) {
            //printf("ibx : %i , %.8f /n", ibx, muHistPerBX[ibx+1]);
            type = 0;
            mod4 = ibx % 4;
            
            for (jbx = ibx + 1; jbx < ibx + bx_len; jbx++) {
                
                // Define correction
                if (type < 4) {
                    cor = type1[type];
                    quadratic = order1[type];
                } else {
                    cor = 0;
                    quadratic = 0;
                }

                if ( jbx < bx_len ) {
                    muHistPerBX[jbx] -= ((muHistPerBX[ibx] * HFSBR[jbx-ibx]) + (muHistPerBX[ibx] * cor + muHistPerBX[ibx]*muHistPerBX[ibx] * quadratic));
                } else {
                    muHistPerBX[jbx-bx_len] -= ((muHistPerBX[ibx] * HFSBR[jbx-ibx]) + (muHistPerBX[ibx] * cor + muHistPerBX[ibx]*muHistPerBX[ibx] * quadratic));
                }

                type += 1;
            }
        }
    }
}

void apply_reflect(int * activeBXMask, float * muHistPerBX, float * SBR) {

    int ibx, jbx;
    int bx_len = 3564;
    int jbx_len = 50;
    
    for(ibx=0; ibx < bx_len - 100; ibx++) {
        if (activeBXMask[ibx] == 1) {
            for (jbx=0; jbx < jbx_len; jbx++) {
                //printf("type : %i, frac: %.8f /n", jbx, cor);
                muHistPerBX[ibx+jbx] -= SBR[jbx];//*1000;
            }
        }
    }

}

void revert_afterglow( int * activeBXMask, float * muHistPerBX, float * HFSBR) {

    // Calculate
    int ibx, jbx;
    int bx_len = 3564;
    int sbr_len = 3564;
    
    for (ibx = bx_len ; ibx-- > 0 ; ) {
    //for(ibx=0; ibx < bx_len; ibx++) {
        if (activeBXMask[ibx] == 1) {
            //printf("ibx : %i , %.8f /n", ibx, muHistPerBX[ibx+1]);
            for (jbx = ibx + 1; jbx < ibx + sbr_len; jbx++) {
                if ( jbx < bx_len ) {
                    muHistPerBX[jbx] += muHistPerBX[ibx] * HFSBR[jbx-ibx];
                } else {
                    muHistPerBX[jbx-bx_len] += muHistPerBX[ibx] * HFSBR[jbx-ibx];
                }
            }
        }
    }
}

void calculate_pedestal( float * muHistPerBX, float * pedestal ) {

    int nSample=13;
    //int bx_len = 3564;
    int ibx, jbx;
    
    for (ibx = 0; ibx < 4; ibx++){
        for (jbx = ibx; jbx < 4*nSample; jbx+=4) {
            pedestal[ibx] +=  muHistPerBX[3500 + jbx]/nSample;
        }
    }
}

void subtract_pedestal( float * muHistPerBX, float * pedestal ) {

    int nSample=10;
    int bx_len = 3564;
    int ibx, jbx;
    
    for (ibx = 0; ibx < 4; ibx++){
        for (jbx = ibx; jbx < 4*nSample; jbx+=4) {
            pedestal[ibx] +=  muHistPerBX[3500 + jbx]/nSample;
        }
    }
    
    for(ibx = 0; ibx < bx_len; ibx++ ) {
        muHistPerBX[ibx] -= pedestal[ibx % 4];
    }
}

void subtract_fixed_pedestal( float * muHistPerBX, float * pedestal ) {

    int bx_len = 3564;
    int ibx;
    
    for(ibx = 0; ibx < bx_len; ibx++ ) {
        muHistPerBX[ibx] -= pedestal[ibx % 4];
    }
}