import pandas
import numpy as np
import matplotlib.pyplot as plt
import glob
import tables

# custom modules
from pybrildpg import hf_correction
from pybrildpg import plotter
from pybrildpg import hd5_reader

# BLM PART
def read_bcml():
    
    df = pandas.concat([pandas.read_csv(filepath
                  , sep = "\t", names=['fill', 'run', 'list']) for filepath in glob.iglob('input/bcml_fill_data/*.csv')])
        
    return df.sort_values(by='fill', ascending=True)

def read_bcml_fill(fill_num, fill_list):
    
    sliced_df = fill_list.loc[fill_list['fill'] == fill_num].list.to_list()
    file_path = sum([x.replace(' ', '').split(',') for x in sliced_df], [])
    df = pandas.concat([read_bcml_file(filepath, fill_num) for filepath in file_path])
    
    return df

def read_bcml_file(file_path, fill):
    
    with tables.open_file(file_path, 'r') as table:
        data = pandas.DataFrame( data=table.root['bcm'][:].tolist()
                               , columns=table.root['bcm'].colnames)
        
        data = data[['fillnum', 'runnum', 'lsnum', 'nbnum', 'acq']]
        acq = np.stack(data['acq'])
        #data['BLM_pZ'] = acq[:, 284].tolist()
        #data['BLM_mZ'] = acq[:, 572].tolist()

        ind1 = 9 + 12*34
        ind2 = 9 + 12*47

        data['BLM_pZ'] = acq[:, ind1].tolist()
        data['BLM_mZ'] = acq[:, ind2].tolist()
        
        data = data[data.fillnum == fill]
        
        return data[['fillnum', 'runnum', 'lsnum', 'nbnum', 'BLM_pZ', 'BLM_mZ']]

def check_blm_linearity(df, lumi, station):
    
    fig = plotter.create_figure(f'Instantaneous Luminosity ({lumi}) [Hz/ub]', f'Ratio wrt {station}', 'BLM')
    
    df['ratio'] = np.nan_to_num(df['avgraw']/df[station], nan=0, posinf=0, neginf=0 ).tolist()
    
    #df = df[df['ratio'].between(0.75, 1.25, inclusive=False)]
    
    df['SBIL'] = df['avgraw']/number_of_active
    
    # Raw
    plt.plot(df['SBIL'], df['ratio'], '.', markersize = 0.5, alpha = 0.5, label=f'Raw ratio')
    
    # Fitting only for shorter range. TODO: Convert to SBIL
    #df = df[df['SBIL'].between(3, 6, inclusive=False)]
    
    # Do binning
    '''
    y_bins, bin_edges, misc = binned_statistic(df['SBIL'], df['ratio'], statistic=np.mean, bins=40)
    s_bins, bin_edges, misc = binned_statistic(df['SBIL'], df['ratio'], statistic=np.std, bins=40)
    x_bins = (bin_edges[:-1] + bin_edges[1:])/2
    
    plt.errorbar(x_bins, y_bins, yerr = s_bins, fmt = 'o-', linestyle='', markersize=4, lw=1, zorder=10, capsize=3, capthick=1, label=f'{lumi}/{station}')
    
    gradient, intercept, r_value, p_value, std_err = stats.linregress(x_bins, y_bins)
    lin_fit = np.array(x_bins) * gradient + intercept
    
    plt.plot(x_bins, lin_fit, linestyle='--', linewidth = 3, color = 'r', alpha = 0.9)
    plt.plot([], [], ' ', label = 'Gradient = {}'.format(round(gradient*100, 5)))
    plt.plot([], [], ' ', label = 'Intercept = {}'.format(round(intercept, 5)))
    plt.plot([], [], '', label = f'Fill {fill}')
    '''
    
    #plt.ylim(0.003, 0.005)
    plt.legend(loc='upper right')

# ====================================================================
# Main script to extract corrections and generate plots
# for per fill data
# ====================================================================
beam_path = '/brildata/24/'
file_path = '/brildata/24/'

node = 'hfetlumi'#'hfetlumi' hfoclumi

stable_fills = [int(x.replace(file_path, '')) for x in glob.iglob(f'{file_path}/*', recursive = True)]
stable_fills.sort()

print(stable_fills)

# Find 
fill = 10169 # 9003 - single 9004, 9008

# Read beam data
beam = hd5_reader.read_beam_fill(beam_path, fill, 'beam')
beam_slice = beam[['fillnum', 'runnum', 'lsnum', 'nbnum', 'status', 'collidable']]

# TODO: replace all this part to generate per Fill masks and number of colliding
# Make sure that the first entry is correspond to the given fill
#beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'STABLE BEAMS')]
beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'ADJUST')]

beam_loc = beam_loc.loc[(beam_loc['fillnum'] == fill)]
activeBXMask = np.array(beam_loc['collidable'].iloc[0])
number_of_active = beam_loc['ncollidable'].iloc[0]

# BEAMS
print(activeBXMask)
print(number_of_active)
print(beam.columns)
print(beam['machinemode'].unique())
print(beam['status'].unique())


# Read lumi data
lumi = hd5_reader.read_fill(file_path, fill, node)

df = pandas.merge( lumi
                 , beam_slice
                 , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])


#df = df.loc[(df['status'].str.decode("utf-8") == 'STABLE BEAMS')]
#df = df[10000:15000]

# Useful to have that for some algos
#df['sbil'] = np.array([np.multiply(hist, activeBXMask).sum()/number_of_active for hist in np.stack(df['bxraw'])]).tolist()
#df = df.round({'sbil': 5})
#df = df.sort_values(by=['sbil'])

bcml_list = read_bcml()
print(bcml_list)
# Try to read bcml data

bcml = read_bcml_fill(fill, bcml_list)
        
df = pandas.merge( df
                 , bcml
                 , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])

# 'BLM_pZ', 'BLM_mZ'
check_blm_linearity(df, node, 'BLM_mZ')
check_blm_linearity(df, node, 'BLM_pZ')

plt.show()