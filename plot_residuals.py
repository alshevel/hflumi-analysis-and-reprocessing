import numpy as np
import matplotlib.pyplot as plt

# custom modules
from pybrildpg import plotter
from pybrildpg import hd5_reader

# ====================================================================
# Main script to extract corrections and generate plots
# for per fill data
# ====================================================================

file_path = '/localdata/alshevel/hf_studies/residuals_hd5/hfetlumi'
node = 'residuals'

# Read lumi data
df = hd5_reader.read_custom_data(file_path, node)
print(df)
df = df[df.fill > 10290]
#df = df[df.fill == 6847]

#df = df[df.sbil > 0.1]

fig = plotter.create_figure('Instantaneous luminosity [Hz/ub]', 'Type1 residual fraction', 'Fill 6847')
plt.plot(df.sbil * 11245.6/3333, df.type1/df.sbil, '.', alpha = 0.5, label = 'data')
# Do linear fit
#new_x = np.linspace(df.sbil, df.sbil, num=np.size(df.sbil))
#coeffs = np.polyfit(df.sbil, df.type1/df.sbil, 1)
#print(coeffs)
#new_line = np.polyval(coeffs, new_x) 
#plt.plot(new_x, new_line, label = f'f(x) = a*x + b, a = {round(coeffs[0], 4)}, b = {round(coeffs[1], 4)}')
#plt.legend()
#plt.xlim(0, 10)
#plt.ylim(0.02, 0.06)
plt.savefig('residual1.png', dpi = 500)

fig = plotter.create_figure('Instantaneous luminosity [Hz/ub]', 'Type2 residual fraction')
plt.plot(df.sbil, df.type2/df.sbil, '.', alpha = 0.01)
#plt.xlim(0.1, 3)
#plt.ylim(-0.01, 0.01)

plt.show()