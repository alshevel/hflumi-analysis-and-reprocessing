import ctypes
from ctypes import cdll

import numpy as np
# C library for afterglow reprocessing
lib = cdll.LoadLibrary('c-modules/libafterglow.so')

# ====================================================================
# C ROUTINE MODULE (PROCEDURE)
# (Internal to the module)
# ====================================================================
def read_afterglow_corrections(file = '../afterglow/hfet23.txt'):
    with open(file) as f:
        return (ctypes.c_float * 3564)(*[float(x) for x in f.read().split(',')])

def c_subtract_pedestal(mu):
    c_mu = (ctypes.c_float * 3564)(*mu)
    pedestal = [0 for x in range(0, 4)]
    c_ped = (ctypes.c_float * 4)(*pedestal)
    
    lib.subtract_pedestal(c_mu, c_ped)
    
    return np.array(c_mu)

def c_subtract_fixed_pedestal(mu, ped):
    c_mu = (ctypes.c_float * 3564)(*mu)
    c_ped = (ctypes.c_float * 4)(*ped)
    
    lib.subtract_fixed_pedestal(c_mu, c_ped)
    
    return np.array(c_mu)

def c_calculate_pedestal(mu):
    c_mu = (ctypes.c_float * 3564)(*mu)
    pedestal = [0 for x in range(0, 4)]    
    c_ped = (ctypes.c_float * 4)(*pedestal)

    lib.calculate_pedestal(c_mu, c_ped)
    
    return np.array(c_ped)

def c_afterglow(mask, mu, c_hfsbr, type1, order1, apply = True):
    # Function to revert and apply new SBR corrections
    # Code is a part of afterglow.c
    c_mask = (ctypes.c_int * 3564)(*mask)
    c_mu = (ctypes.c_float * 3564)(*mu)
    c_type1 = (ctypes.c_float * 4)(*type1)
    c_order1 = (ctypes.c_float * 4)(*order1)

    if apply:
        lib.calculate_afterglow(c_mask, c_mu, c_hfsbr, c_type1, c_order1)
    else:
        lib.revert_afterglow(c_mask, c_mu, c_hfsbr, c_type1, c_order1)
    
    return np.array(c_mu)


# ====================================================================
# Python/C Corrections MODULE
# (External)
# ====================================================================
def revert_afterglow(df, activeBXMask, correction_path, type1 = [0,0,0,0], order1 = [0,0,0,0]):
    hbsrt = read_afterglow_corrections(correction_path)
    
    hists = np.stack(df.bxraw)
    new_hist = [c_afterglow(activeBXMask, hist, hbsrt, type1, order1, False) for hist in hists]
    df.bxraw = new_hist
 
def apply_afterglow(df, activeBXMask, type1 = [0,0,0,0], order1 = [0,0,0,0], function = None, p0 = [], path = ''):
    
    if not path == '':
        hbsrt = read_afterglow_corrections(path)
    else:
        hbsrt = (ctypes.c_float * 3564)(*[function(x, *p0) for x in range(3564)])
    
    hists = np.stack(df.bxraw)
    new_hist = [c_afterglow(activeBXMask, hist, hbsrt, type1, order1, True) for hist in hists]
    df.bxraw = new_hist

# Maybe it makes more sense
def subtract_flat_top(df, df_flat_top, plot = False):
    # Extract FLAT TOP data
    flat_top_mean = np.stack(df_flat_top.bxraw).mean(axis = 0)

    new_hist = [hist-flat_top_mean for hist in np.stack(df.bxraw)]
    df.bxraw = new_hist

def subtract_flat_top_pedestal(df, df_flat_top):
    
    ped = calc_pedestal(df_flat_top)
    hists = np.stack(df.bxraw)
    df.bxraw = [c_subtract_fixed_pedestal(hists[x], ped) for x in range(len(hists))]
    
def subtract_pedestal(df):
    hists = np.stack(df.bxraw)
    df.bxraw = [c_subtract_pedestal(hist) for hist in hists]

def calc_pedestal(df):
    hists = np.stack(df.bxraw)
    ped = [c_calculate_pedestal(hist) for hist in hists]
    return np.array(ped).mean(axis = 0)

def revert_pedestal(df):
    # Pedestal order is correct
    hists = np.stack(df.bxraw)
    new_hist = []
    for hist in hists:
        ped = [hist[3556], hist[3553], hist[3554], hist[3555]]
        pedestal_row = np.array([ped[x % 4] for x in range(3564)])
        hist = hist - pedestal_row
        new_hist.append(hist)
    
    df.bxraw = new_hist

def revert_fixed_pedestal(df):
    # Pedestal order is correct
    hists = np.stack(df.bxraw)
    ped_hists = np.stack(df.ped)

    df.bxraw = [c_subtract_fixed_pedestal(hists[x], -1 * ped_hists[x]) for x in range(len(hists))]
