import tables
import glob, pathlib
import pandas

# Could be extended by alternative tables
class DefaultLumitable(tables.IsDescription):
    fillnum = tables.UInt32Col(shape=(), dflt=0, pos=0)
    runnum = tables.UInt32Col(shape=(), dflt=0, pos=1)
    lsnum = tables.UInt32Col(shape=(), dflt=0, pos=2)
    nbnum = tables.UInt32Col(shape=(), dflt=0, pos=3)
    timestampsec = tables.UInt32Col(shape=(), dflt=0, pos=4)
    timestampmsec = tables.UInt32Col(shape=(), dflt=0, pos=5)
    totsize = tables.UInt32Col(shape=(), dflt=0, pos=6)
    publishnnb = tables.UInt8Col(shape=(), dflt=0, pos=7)
    datasourceid = tables.UInt8Col(shape=(), dflt=0, pos=8)
    algoid = tables.UInt8Col(shape=(), dflt=0, pos=9)
    channelid = tables.UInt8Col(shape=(), dflt=0, pos=10)
    payloadtype = tables.UInt8Col(shape=(), dflt=0, pos=11)
    calibtag = tables.StringCol(itemsize=32, shape=(), dflt='', pos=12)
    avgraw = tables.Float32Col(shape=(), dflt=0.0, pos=13)
    avg = tables.Float32Col(shape=(), dflt=0.0, pos=14)
    bxraw = tables.Float32Col(shape=(3564,), dflt=0.0, pos=15)
    bx = tables.Float32Col(shape=(3564,), dflt=0.0, pos=16)
    maskhigh = tables.UInt32Col(shape=(), dflt=0, pos=17)
    masklow = tables.UInt32Col(shape=(), dflt=0, pos=18)

# ====================================================================
# HD5 extraction MODULE
# ====================================================================
# Read beam information
def read_beam_fill(path: str, fill: int, node: str):
    return pandas.concat([read_beam(f, node) for f in pathlib.Path(f'{path}/{fill}/').glob('*.hd5')])
    
def read_beam(file_path, node):
    with tables.open_file(file_path, 'r') as table:
        try:
            data = pandas.DataFrame( data=table.root[node][:].tolist()
                                   , columns=table.root[node].colnames)
        except:
            data = pandas.DataFrame()

    return data

# Read list of Fills. Consider to remove to separate file to reuse for all lumistore
def read_fill(path: str, fill: int, node: str):
    return pandas.concat([read_run(f, node) for f in glob.glob(f'{path}/{fill}/*.hd5', recursive=True)])

def read_run(file_path, node):
    with tables.open_file(file_path, 'r') as table:
            data = pandas.DataFrame( data=table.root[node][:].tolist()
                                   , columns=table.root[node].colnames)
    table.close()
    
    return data

def read_custom_data(path: str, node: str):
    return pandas.concat([pandas.read_hdf(f, node) for f in glob.glob(f'{path}/*.hd5', recursive=True)])

# Specific readers to have lightweight hd5
# TODO: ideally should be configurable
def read_custom_fill(path: str, fill: int, node: str, field = 'ncollidable'):
    return pandas.concat([read_file_only_raw(f, node, field) for f in pathlib.Path(f'{path}/{fill}/').glob('*.hd5')])

def read_file_only_raw(file_path, node, field):

    with tables.open_file(file_path, 'r') as table:
        if node == 'beam':
            try:
                data = pandas.DataFrame( data = table.root[node][:][field].tolist()
                                    , index = table.root[node][:]['timestampsec'].tolist()
                                    , columns = [field])
            except:
                data = pandas.DataFrame()
                                   
        else:
            data = pandas.DataFrame( data = table.root[node][:]['avgraw'].tolist()
                                   , index = table.root[node][:]['timestampsec'].tolist()
                                   , columns = [node])

    table.close()        
    
    return data

# ====================================================================
# HD5 Processing module
# ====================================================================
def save_to_hd5(df, node, path, name):

    # Writing to output tables
    outtablename = node
    compr_filter = tables.Filters(complevel=9, complib='blosc')
    chunkshape=(100,)
                
    h5out = tables.open_file(f'{path}{name}', mode='w')
    outtable = h5out.create_table( '/'
                                 , outtablename
                                 , DefaultLumitable
                                 , filters = compr_filter
                                 , chunkshape = chunkshape)
                        
    rownew = outtable.row
    for index, oldrow in df.iterrows():
        for entry in DefaultLumitable.__dict__['columns']:
            rownew[entry] = oldrow[entry]
        rownew.append()
                        
    h5out.close()