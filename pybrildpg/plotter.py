import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import os

from scipy import stats
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic

# ====================================================================
# Fit Functions MODULE
# ====================================================================
def complex_fit(x, a1, b1, c1, a2, b2, c2, a3, b3, c3):
    return (a1 * np.exp(-b1 * x) + c1) + (a2 * np.exp(-b2 * x) + c2) + (a3*np.exp(-((x-b3)/c3)**2))

def exp_fit(x, a1, b1, c1):
    return (a1 * np.exp(-b1 * x) + c1)

def double_exp_fit(x, a1, b1, c1, a2, b2, c2):
    return (a1 * np.exp(-b1 * x) + c1) + (a2 * np.exp(-b2 * x) + c2)

def perform_fit(bx, hist, start, end, func, p0):
    # Final exponent
    params, cv = curve_fit(func, bx[start:end], hist[start:end], p0)
    print([x for x in params])
    return params

def do_linear_fit(x_value, y_value):
    gradient, intercept, r_value, p_value, std_err = stats.linregress(x_value, y_value)
    lin_fit = np.array(x_value) * gradient + intercept
    
    return lin_fit, gradient, intercept, r_value, p_value, std_err
    
def do_binning(x_value, y_value, nbins):
    y_bins, bin_edges, misc = binned_statistic(x_value, y_value, statistic=np.mean, bins=nbins)
    s_bins, bin_edges, misc = binned_statistic(x_value, y_value, statistic=np.std, bins=nbins)
    x_bins = (bin_edges[:-1] + bin_edges[1:])/2

    return x_bins, y_bins, s_bins

# ====================================================================
# Plotting MODULE
# ====================================================================

# Default plot styling
def create_figure(x_axis, y_axis, top_label = '2024 (13.6 TeV)', plot_type = 'Private'):
    fig = plt.figure()
    plt.xlabel(x_axis, fontsize=12, fontname='Helvetica')
    plt.ylabel(y_axis, fontsize=12, fontname='Helvetica')
    ax = fig.gca()
    ax.minorticks_on()
    ax.tick_params(bottom=True, top=True, left=True, right=True, direction='in', which='both')
    ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False, direction='in', which='both')
    plt.rcParams['font.family'] = 'Helvetica'
    ax.text(0.05, 0.95, '$\\bf{}$ $\\it{}$'.format('CMS', plot_type), transform=ax.transAxes, ha='left', va='top')
    ax.text(1, 1.05, top_label, transform=ax.transAxes, ha='right', va='top')
    
    return fig

# ====================================================================
# Per FILL Functions
# ====================================================================

# Plot pileup
def plot_pile_dist(df, label_r, fill):
    #fig = create_figure('BCID', 'Rates')
    fig = create_figure('Interactions per crossing (pile-up)', '', 'CMS Pile-up distribution', 'Private')
    #plt.plot([x for x in range(3564)], hist, '.', label = label_r)
    #plt.plot([], [], '', label = f'Fill {fill}')

    pileup = np.stack(df.bxraw).flatten() # .max(axis = 0)
    pileup = pileup[pileup > 0.5]

    plt.hist(pileup * 11245.6/3200 * 7.12, range = [0, 200], bins = 100, label = label_r)
    case = u'pp ${\sigma_{inel}}$'
    plt.plot([], [], '.', label = f'{case} = 80 mb')
    
    #plt.xlim(2565, 2625)
    #plt.ylim(0, 240)

    plt.legend(loc='upper right')
    plt.savefig('hfet_dist.png', dpi = 500)

def plot_hist_bx(hist, label_r, fill, log = False):
    
    fig = create_figure('BCID', 'Rates')
    plt.plot([x for x in range(3564)], hist, '.', label = label_r)
    plt.plot([], [], '', label = f'Fill {fill}')

    case = u'pp ${\sigma_{inel}}$'
    plt.plot([], [], '.', label = f'{case} = 80 mb')

    if log:
        plt.yscale('log')
    
    plt.legend()
    plt.legend(loc='upper right')

def plot_instant(df, activeBXMask):
    
    hists = np.stack(df['bxraw'])
    avg = np.array([np.multiply(hist, activeBXMask).sum() for hist in hists])
    df[f'calc_avg'] = avg.tolist()
    
    fig = create_figure( 'Timestamp UTC [s]', 'Instantenious luminosity [Hz/ub]')
    plt.plot(df.index, df['avgraw'], '.', label = 'avg_published')
    plt.plot(df.index, df['calc_avg'], '.', label = 'avg_calculated')
    #plt.plot([], [], '', label = f'Fill {fill}')
    plt.legend()
    
    max_x = df['avgraw'].max()
    plt.ylim(0, 1.2 * float(max_x))
    
    fig = create_figure( 'Timestamp UTC [s]', 'Ratio')
    plt.plot(df.timestampsec, df['avgraw']/df['calc_avg'], '.', label = 'avg_published')
    plt.ylim(0.5, 1.5)
    plt.legend()

# Plot and calculate Type1 and Type2 in one fill
# TODO: add output to the fill data in the raw files
# so we could combine
def calculate_residual(df, activeBXMask, fill, node, save_path = '/localdata/alshevel/hf_studies', plot = True, hd5_save = True, label_ext = ''):

    # Add function to save figures per fill and generate hd5 output
    df = df[df.sbil > 0]

    hists = np.stack(df['bxraw'])
    sbil = np.stack(df.sbil)
    fill_array = np.array([fill for x in range(len(hists))])
    
    type1_value = [bx+1 for bx in range(3480) if (activeBXMask[bx] == 1 and activeBXMask[bx+1] == 0)]
    type2_value = [bx for bx in range(3480) if (activeBXMask[bx] == 0 and bx not in type1_value)]
    
    type1 = np.array([hists[x][type1_value].mean(axis = 0) for x in range(len(hists))])
    type2 = np.array([hists[x][type2_value].mean(axis = 0) for x in range(len(hists))])

    if plot:
        # Save figures
        save_path_type1 = f'{save_path}/type1_plots/{node}'
        if not os.path.isdir(save_path_type1):
            os.makedirs(save_path_type1)

        save_path_type2 = f'{save_path}/type2_plots/{node}'
        if not os.path.isdir(save_path_type2):
            os.makedirs(save_path_type2)

        # Type1
        fig = create_figure('Instantaneous luminosity [Hz/ub]', 'Type1 residual fraction', f'Fill {fill}')
        plt.plot(sbil, type1, '.')
        plt.savefig(f'{save_path_type1}/{fill}_type1.png', dpi = 300)
        
        # Type2
        fig = create_figure('Instantaneous luminosity [Hz/ub]', 'Type2 residual fraction', f'Fill {fill}')
        plt.plot(sbil, type2, '.')
        plt.savefig(f'{save_path_type2}/{fill}_type2.png', dpi = 300)


    if hd5_save:

        save_path_hd5 = f'{save_path}/residuals_hd5/{node}'
        if not os.path.isdir(save_path_hd5):
            os.makedirs(save_path_hd5)

        df_residual = pd.DataFrame({'fill': fill_array, 'sbil': sbil, 'type1': type1, 'type2': type2})
        df_residual.to_hdf(f'{save_path_hd5}/{fill}.hd5', key='residuals', mode='w')
        print(df_residual)


def plot_pedestal(df, fill):
    
    hists = np.stack(df.bxraw)
    sbil = np.stack(df.sbil).flatten()
    
    bcids = [3560, 3561, 3562, 3563]
    #bcids = [3499 + x for x in range(4)]
    
    fig = create_figure(f'SBIL', f'Pedestal ADC', f'Fill {fill}')
    for bcid in bcids:
        
        bx_value = np.array([hist[bcid] for hist in hists]).flatten()
        
        plt.plot(sbil, bx_value, '.', label = f'{bcid}')

        # Do linear fit
        new_x = np.linspace(min(sbil), max(sbil), num=np.size(sbil))
        coeffs = np.polyfit(sbil, bx_value, 1)
        print(coeffs)
        new_line = np.polyval(coeffs, new_x)
                        
        plt.plot(new_x, new_line, label = f'f(x) = a*x + b, a = {round(coeffs[0], 4)}, b = {round(coeffs[1], 4)}')
        plt.legend()

    # Combined bcids
    fig = create_figure(f'SBIL', f'Pedestal ADC', f'Fill {fill}')
    bx_value = np.array([hist[bcids].mean() for hist in hists]).flatten()
    
    plt.plot(sbil, bx_value/sbil, '.', label = f'{bcid}')

    # Do linear fit
    new_x = np.linspace(min(sbil), max(sbil), num=np.size(sbil))
    coeffs = np.polyfit(sbil, bx_value/sbil, 1)
    print(coeffs)
    new_line = np.polyval(coeffs, new_x)
                    
    plt.plot(new_x, new_line, label = f'f(x) = a*x + b, a = {round(coeffs[0], 4)}, b = {round(coeffs[1], 4)}')
    plt.legend()


    fig = create_figure(f'Time', f'Pedestal ADC', f'Fill {fill}')
    for bcid in bcids:

        bx_value = np.array([hist[bcid] for hist in hists]).flatten()
        
        plt.plot(df.index, bx_value, '.', label = f'{bcid}')
        plt.legend()

    fig = create_figure(f'ADC counts renormalized by number of orbits', f'Entries', f'Fill {fill}')
    for bcid in bcids:

        bx_value = np.array([hist[bcid] for hist in hists]).flatten()
        plt.hist(bx_value, bins = 500, range = [0.015, 0.025])
        
    #plt.savefig('pedestal_subtracted.png', dpi = 500)


# Calculates the residual corrections for mod4
def calculate_quad_type1(df, activeBXMask, type, order = 1, mod4_a = False):
    
    hists = np.stack(df.bxraw)
    
    if mod4_a:
        mod4 = [0, 1, 2, 3]
    else:
        mod4 = [0]
    
    p0 = []; p1 = []; p2 = []

    for mod in mod4:                    
        try:
            print(f'Processing type {type}')
                
            # Require all bunches to be non-colliding
            if mod4_a:
                colliding_value = [bx for bx in range(3564) if (activeBXMask[bx] == 1 and activeBXMask[bx+type] == 0 and bx % 4 == mod)]
                afterglow_value = [bx+type for bx in range(3564) if (activeBXMask[bx] == 1 and activeBXMask[bx+type] == 0 and bx % 4 == mod)]
            else:
                colliding_value = [bx for bx in range(3564) if (activeBXMask[bx] == 1 and activeBXMask[bx+type] == 0)]
                afterglow_value = [bx+type for bx in range(3564) if (activeBXMask[bx] == 1 and activeBXMask[bx+type] == 0)]
                
            bx_value = np.array([hist[colliding_value] for hist in hists]).flatten()
            bx_type1 = np.array([hist[afterglow_value] for hist in hists]).flatten()
            
            # Fitters
            x_bins, y_bins, s_bins = do_binning(bx_value, bx_type1, 20)
            
            # Do quadratic fit
            new_x = np.linspace(min(bx_value), max(bx_value), num=np.size(bx_value))
            coeffs = np.polyfit(bx_value, bx_type1, order)
            new_line = np.polyval(coeffs, new_x)
            
            if order == 1:
                p0.append(0); p1.append(coeffs[0]); p2.append(coeffs[1])
            elif order == 2:
                p0.append(coeffs[0]); p1.append(coeffs[1]); p2.append(coeffs[2])                
            
            #fig = plt.figure()
            fig = create_figure('Instantaneous luminosity [Hz/ub]', 'Type1 residual fraction', f'Type1 for mod{mod}')
            
            plt.plot( bx_value, bx_type1, '.', alpha = 0.5)
            plt.errorbar(x_bins, y_bins, yerr = s_bins, fmt = 'o-', linestyle='', markersize=4, lw=1, zorder=10, capsize=3, capthick=1)
            plt.plot(new_x, new_line, label = f'Quadratic fit ax2 + bx + c')
            plt.plot([], [], '.', label = f'a = {round(coeffs[0], 5)}')
            plt.plot([], [], '.', label = f'b = {round(coeffs[1], 5)}')
            plt.plot([], [], '.', label = f'c = {round(coeffs[2], 5)}')
            plt.legend(loc='lower right')

        except:
            print('Quadratic fit failed')

            p0.append(0)
            p1.append(0)
            p2.append(0)

        #save_path_loc = save_path + 'type1'
        #if not os.path.isdir(save_path_loc):
        #    os.makedirs(save_path_loc)

        #plt.savefig(f'figures/quadratic/figure_quad_{fill}_{type}_{mod}.png', dpi = 500)
    return p0, p1, p2

# Useful for single bunch fills
def plot_type2(df, activeBXMask, number_of_active):

    # Remove laser and zero pedestal
    bx_to_clean = [3488, 3489, 3490, 3491, 3556, 3553, 3554, 3555]

    # roll mask to align zero with first colliding BX
    leadingBX = np.diff(np.array(activeBXMask).astype(int), axis = 0)
    first_element = next((i for i, j in enumerate(leadingBX) if j), None)
    
    if number_of_active > 1:
        activeBXMask = np.roll(activeBXMask, -first_element-1)
        
    trains = np.split(activeBXMask, np.where(activeBXMask[:-1] == 1)[0])
    
    # Starting from bcid Zero
    bcid = 0
    new_trains = []

    for train in trains:
        temp = []
        for hit in train:
            temp.append(bcid)
            bcid += 1
        if temp:
            new_trains.append(temp)

    trains = new_trains
    
    pileup = 0.000001
    loc_df = df[df['sbil'] > pileup]

    # Set plotting
    fig = create_figure('BCID', 'SBR (Afterglow)')

    # Do mean histogram and renormalize
    mean_hist = np.stack(loc_df.bxraw).mean(axis = 0)

    if number_of_active > 1:
        mean_hist = [np.roll(mean_hist, -first_element-1)][0]

    for position, train in enumerate(trains):

        bunch_indices = []
        lumi_value = []

        # Check if bunch has large enough lumi comparable
        # to the mean pileup
        sbr_value = mean_hist[train[0]]

        if sbr_value < pileup:
            continue

        counter = 0

        for bcid in train:        

            if bcid not in bx_to_clean:
                bunch_indices.append(counter)               
                lumi_value.append(mean_hist[bcid]/sbr_value)
                counter += 1

        if len(bunch_indices) < 500:
            continue
        
        plt.plot( bunch_indices, lumi_value, '.', label = f'BCID {train[0]}')

        try:        
            '''
            a1, b1, c1, a2, b2, c2, a3, b3, c3 = perform_fit( bunch_indices
                                                            , lumi_value
                                                            , 4 # skip first 4 bcid
                                                            , len(bunch_indices)
                                                            , complex_fit
                                                            , (0.00010, 0.001, 0, 0.002, 0.18, 0, 9.6e-05, 74, 40))
            plt.plot( bunch_indices
                    , complex_fit(np.array(bunch_indices), a1, b1, c1, a2, b2, c2, a3, b3, c3)
                    )
            '''
            '''
            a1, b1, c1 = perform_fit( bunch_indices
                                    , lumi_value
                                    , 4 # skip first 4 bcid
                                    , 600
                                    , exp_fit
                                    , (0.00010, 0.001, 0))
            plt.plot( bunch_indices
                    , exp_fit(np.array(bunch_indices), a1, b1, c1)
                    )
            
            '''
            a1, b1, c1, a2, b2, c2 = perform_fit( bunch_indices
                                                , lumi_value
                                                , 6 # skip first 4 bcid
                                                , 1500
                                                , double_exp_fit
                                                , (0.00010, 0.001, 0, 0.002, 0.18, 0))
            plt.plot( bunch_indices
                    , double_exp_fit(np.array(bunch_indices), a1, b1, c1, a2, b2, c2)
                    )
            
            
            f_values = [x for x in double_exp_fit(np.array(bunch_indices), a1, b1, c1, a2, b2, c2)]
            #print(f_values)
            #print(len(f_values))

            if len(f_values) < 3564:
                zeros = [0 for x in range(3564-len(f_values))]
                f_values = f_values + zeros
            
            print(f_values)
            print(len(f_values))

            #plt.ylim(1e-5, 1.02)
            #plt.yscale('log')
            plt.legend()

        except:
            print(f'fit failed for bcid {position}')

# ====================================================================
# Comparison Plots (DPG)
# ====================================================================

# Simple ratio for one fill
def ratio_plot(data1, data2, first_d, second_d, fill):
    
    try:
    
        fig = create_figure( 'Time'
                           , 'Ratio'
                           , f'FILL {fill}'
                           )
    
        print('Processing Fill {}'.format(fill))
            
        if data1.empty or data2.empty:
            print('Missing data for one of the detectors in fill: {}'.format(fill))
            
        data1 = data1.rename(columns={first_d['node']: first_d['alias']})
        data1 = data1[data1[first_d['alias']] > 0]
        data1[first_d['alias']] = data1[first_d['alias']] * 11245.6 / first_d['sigvis']
                        
        data2 = data2.rename(columns={second_d['node']: second_d['alias']})
        data2 = data2[data2[second_d['alias']] > 0]
        data2[second_d['alias']] = data2[second_d['alias']] * 11245.6 / second_d['sigvis']
        
        # Fitter
        df = data1.join(data2)
        df = df.loc[~(df==0).all(axis=1)].dropna()
        
        df['ratio'] = df[first_d['alias']]/df[second_d['alias']]
        df = df[df.ratio.between(0.5, 1.5, inclusive=False)].reset_index()
        
        plt.plot(df['ratio'], '.', label = 'Ratio {}/{}'.format(first_d['alias'], second_d['alias']))
        plt.ylim(0.8, 1.2)
        plt.legend()
        
        #save_path_loc = save_path + 'ratio'
        #if not os.path.isdir(save_path_loc):
        #    os.makedirs(save_path_loc)

        #plt.savefig(f'{save_path_loc}/{fill}_{first_d["alias"]}_{second_d["alias"]}.png', dpi = 500)
        
    except:
        print('Failed to process fill {}'.format(fill))


# SBIL Ratio Plot
def sbil_ratio(beam, data1, data2, first_d, second_d, fill, save_path = '/localdata/alshevel/hf_studies'):
    
    try:
        print('Processing Fill {}'.format(fill))
        
        num_of_coll_bunches = beam['ncollidable'].max()
        print(num_of_coll_bunches)
                            
        if data1.empty or data2.empty:
            print('Missing data for one of the detectors in fill: {}'.format(fill))
            
        data1 = data1.rename(columns={first_d['node']: first_d['alias']})
        data1 = data1[data1[first_d['alias']] > 0]
        data1[first_d['alias']] = data1[first_d['alias']]/num_of_coll_bunches * 11245.6 / first_d['sigvis']
                 
        data2 = data2.rename(columns={second_d['node']: second_d['alias']})
        data2 = data2[data2[second_d['alias']] > 0]
        data2[second_d['alias']] = data2[second_d['alias']]/num_of_coll_bunches * 11245.6 / second_d['sigvis']
        
        # Fitter
        df = data1.join(data2)
        df = df.loc[~(df==0).all(axis=1)].dropna()
        
        df['sbil'] = df[first_d['alias']]
        df['ratio'] = df[first_d['alias']]/df[second_d['alias']]
        
        # TODO: allowed sbil should be configurable
        df = df[df.sbil > 1]
        df = df[df.ratio.between(0.75, 1.25, inclusive=False)].reset_index()
        
        # Plot SBIL Ratio
        fig = create_figure( 'SBIL [Hz/ub]'
                           , f'{first_d["alias"]}/{second_d["alias"]} Ratio'
                           , f'FILL {fill}'
                           )
        
        plt.plot(df['sbil'], df['ratio'], '.', alpha = 0.1, label = 'Ratio {}/{}'.format(first_d['alias'], second_d['alias']))
        
        gradient, intercept, r_value, p_value, std_err = stats.linregress(df['sbil'], df['ratio'])
        lin_fit = np.array(df['sbil']) * gradient + intercept
        plt.plot(df['sbil'], lin_fit, linestyle='--', color='r')
        print(gradient, intercept, r_value, p_value, std_err)
        plt.plot([], [], ' ', label = 'Gradient = {}'.format(round(gradient, 6)))
        plt.plot([], [], ' ', label = 'Intercept = {}'.format(round(intercept, 3)))

        # TEST
        # Quadratic fit
        '''
        new_x = np.linspace(min(df['sbil']), max(df['sbil']), num=np.size(df['sbil']))
        coeffs = np.polyfit(df['sbil'], df['ratio'], 2)
        new_line = np.polyval(coeffs, new_x)

        plt.plot(new_x, new_line, label = f'Quadratic fit ax2 + bx + c')
        print(coeffs)
        '''

        #plt.xlim(0, 12)
        #plt.ylim(0.9, 1.1)
        plt.legend()

        ratio = df.ratio.to_numpy()
        sbil = df.sbil.to_numpy()
    
        save_path_hd5 = f"{save_path}/sbil_ratios_hd5/{first_d['node']}_{second_d['node']}"
        if not os.path.isdir(save_path_hd5):
            os.makedirs(save_path_hd5)

        df_ratio = pd.DataFrame({'fill': fill, 'sbil': sbil, 'ratio': ratio})
        df_ratio.to_hdf(f'{save_path_hd5}/{fill}.hd5', key='sbil_ratio', mode='w')

        save_plot_path = f"{save_path}/sbil_ratios_plots/{first_d['node']}_{second_d['node']}"
        if not os.path.isdir(save_plot_path):
            os.makedirs(save_plot_path)

        plt.savefig(f'{save_plot_path}/{fill}.png', dpi = 300)

    except:
        print('Failed to process fill {}'.format(fill))


# Default plotter for entire year using hd5
# Develope a new procedure which will provide stored output once
# and plotting part
def cross_detector_plot(data1, data2, first_d, second_d, fill, save_path = '/localdata/alshevel/hf_studies'):
    
    try:
        print('Processing Fill {} for {} and {}'.format(fill, first_d['node'], second_d['node']))
        
        if data1.empty or data2.empty:
            print('Missing data for one of the detectors in fill: {}'.format(fill))
        
        data1 = data1.rename(columns={first_d['node']: first_d['alias']})
        data1[first_d['alias']] = data1[first_d['alias']] * 11245.6 / first_d['sigvis']
        data1 = data1[data1[first_d['alias']] > 0]
            
        data2 = data2.rename(columns={second_d['node']: second_d['alias']})
        data2[second_d['alias']] = data2[second_d['alias']] * 11245.6 / second_d['sigvis']
        data2 = data2[data2[second_d['alias']] > 0]
        
        df = data1.join(data2)
        df = df.loc[~(df==0).all(axis=1)].dropna()

        df['ratio'] = df[first_d['alias']]/df[second_d['alias']]
        df = df[df.ratio.between(0.75, 1.25, inclusive=False)]
                
        mean = np.array([df.ratio.mean()])
        std = np.array([df.ratio.std()])
    
        save_path_hd5 = f"{save_path}/ratios_hd5/{first_d['node']}_{second_d['node']}"
        if not os.path.isdir(save_path_hd5):
            os.makedirs(save_path_hd5)

        df_ratio = pd.DataFrame({'fill': fill, 'ratio': mean, 'error': std})
        df_ratio.to_hdf(f'{save_path_hd5}/{fill}.hd5', key='ratio', mode='w')

    except:
        print('Failed to process fill {}'.format(fill))