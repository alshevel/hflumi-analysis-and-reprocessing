import tables
import glob, pathlib
import pandas

def read_bcml():
    
    df = pandas.concat([pandas.read_csv(filepath
                  , sep = "\t", names=['fill', 'run', 'list']) for filepath in glob.iglob('input/bcml_fill_data/*.csv')])
        
    return df.sort_values(by='fill', ascending=True)

def read_bcml_fill(fill_num, fill_list):
    
    sliced_df = fill_list.loc[fill_list['fill'] == fill_num].list.to_list()
    file_path = sum([x.replace(' ', '').split(',') for x in sliced_df], [])
    df = pandas.concat([read_bcml_file(filepath, fill_num) for filepath in file_path])
    
    return df

def read_bcml_file(file_path, fill):
    
    with tables.open_file(file_path, 'r') as table:
        data = pandas.DataFrame( data=table.root['bcm'][:].tolist()
                               , columns=table.root['bcm'].colnames)
        
        data = data[['fillnum', 'runnum', 'lsnum', 'nbnum', 'acq']]
        acq = np.stack(data['acq'])
        data['BLM_pZ'] = acq[:, 284].tolist()
        data['BLM_mZ'] = acq[:, 572].tolist()
        
        # REMOVE
        data['BLM_pZ'] = data['BLM_pZ']*0.00418
        
        data = data[data.fillnum == fill]
        
        return data[['fillnum', 'runnum', 'lsnum', 'nbnum', 'BLM_pZ', 'BLM_mZ']]

def check_blm_linearity(df, lumi, station):
    
    fig = create_figure(f'Instantaneous Luminosity ({lumi}) [Hz/ub]', f'Ratio wrt {station}', '13 TeV Run2 (2015-2018)')
    
    df['ratio'] = np.nan_to_num(df['avgraw']/df[station], nan=0, posinf=0, neginf=0 ).tolist()
    
    #df = df[df['ratio'].between(0.75, 1.25, inclusive=False)]
    
    df['SBIL'] = df['avgraw']/number_of_active
    
    # Raw
    plt.plot(df['SBIL'], df['ratio'], '.', markersize = 0.5, alpha = 0.5, label=f'Raw ratio')
    
    # Fitting only for shorter range. TODO: Convert to SBIL
    #df = df[df['SBIL'].between(3, 6, inclusive=False)]
    
    # Do binning
    '''
    y_bins, bin_edges, misc = binned_statistic(df['SBIL'], df['ratio'], statistic=np.mean, bins=40)
    s_bins, bin_edges, misc = binned_statistic(df['SBIL'], df['ratio'], statistic=np.std, bins=40)
    x_bins = (bin_edges[:-1] + bin_edges[1:])/2
    
    plt.errorbar(x_bins, y_bins, yerr = s_bins, fmt = 'o-', linestyle='', markersize=4, lw=1, zorder=10, capsize=3, capthick=1, label=f'{lumi}/{station}')
    
    gradient, intercept, r_value, p_value, std_err = stats.linregress(x_bins, y_bins)
    lin_fit = np.array(x_bins) * gradient + intercept
    
    plt.plot(x_bins, lin_fit, linestyle='--', linewidth = 3, color = 'r', alpha = 0.9)
    plt.plot([], [], ' ', label = 'Gradient = {}'.format(round(gradient*100, 5)))
    plt.plot([], [], ' ', label = 'Intercept = {}'.format(round(intercept, 5)))
    plt.plot([], [], '', label = f'Fill {fill}')
    '''
    
    #plt.ylim(0.003, 0.005)
    plt.legend(loc='upper right')
    
    if savefig:
        plt.savefig(f'{figure_path}/blm_{fill}.png', dpi = 500)