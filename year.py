import matplotlib.pyplot as plt
import argparse, yaml, glob
import pandas, os

from pybrildpg import hd5_reader
from pybrildpg import plotter

# TODO: Include part to read David's csv with fill statistics

# Main
data_path = '/brildpg/rawdata/17/'
#data_path = '/localdata/alshevel/hf_origin/hfoc/18/'
#data_path = '/brildata/22_updates/23v1/hfet/'
data_path = '/brildata/24/'
save_path = '/localdata/alshevel/hf_studies'

stable_fills = [int(x.replace(data_path, '')) for x in glob.iglob(f'{data_path}/*', recursive = True)]
stable_fills.sort()

print(stable_fills)

# Configure detector
parser = argparse.ArgumentParser(description="The tool is designed to analyse CMS BRIL data and generate DPG relevant plots and comparisons")

parser.add_argument('detector1', type=str, help='First detector in comparison')
parser.add_argument('detector2', type=str, help='Second detector in comparison')
parser.add_argument('config', type=str, help='Configuration file with detector meta data')

args = parser.parse_args()

config_file = args.config

#stable_fills = [10201]

with open(config_file, 'r') as file:
    meta_data = yaml.safe_load(file)

beam_d = meta_data['beam']
first_d = meta_data[args.detector1]
second_d = meta_data[args.detector2]

file_path = f"{save_path}/ratios_hd5/{first_d['node']}_{second_d['node']}"
file_path_sbil = f"{save_path}/sbil_ratios_hd5/{first_d['node']}_{second_d['node']}"

# Processing Part
for fill in stable_fills:
    
    if fill < 10100:
        continue

    try:
        fname = f'{file_path}/{fill}.hd5'
        fname_sbil = f'{file_path_sbil}/{fill}.hd5'

        if os.path.isfile(fname) and os.path.isfile(fname_sbil):
            print(f'Data for {fill} already exits. Skipping...')
            continue

        beam = hd5_reader.read_custom_fill(beam_d['path'], fill, beam_d['node'], 'status')
        beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'STABLE BEAMS')]
        if beam_loc.empty:
            print('No stable beams data for fill {fill}, continuing...')
            continue

        beam = hd5_reader.read_custom_fill(beam_d['path'], fill, beam_d['node'])
        data1 = hd5_reader.read_custom_fill(first_d['path'], fill, first_d['node'])
        data2 = hd5_reader.read_custom_fill(second_d['path'], fill, second_d['node'])
        
        plotter.sbil_ratio( beam
                        , data1, data2
                        , first_d, second_d
                        , fill
                        , save_path )

        plotter.cross_detector_plot( data1, data2
                                    , first_d, second_d
                                    , fill
                                    , save_path )

    except:
        print(f'{fill} has failed')