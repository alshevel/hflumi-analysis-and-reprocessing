import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse, yaml
from scipy import stats

# custom modules
from pybrildpg import plotter
from pybrildpg import hd5_reader

def do_linear_fit(df, ilumi_range = []):
    # do the fit
    if ilumi_range:
        # remove ilumi range from fit
        df = df[df.ilumi.between(ilumi_range[0], ilumi_range[1], inclusive=False)]

    # remove outliers
    df = df[(np.abs(stats.zscore(df)) < 2).all(axis=1)] 

    new_x = np.linspace(min(df.ilumi), max(df.ilumi), num=np.size(df.ilumi))
    coeffs = np.polyfit(df.ilumi, df.ratio, 1)
    new_line = np.polyval(coeffs, new_x)

    print(new_x, new_line)

    if ilumi_range:
        plt.plot( new_x
                , new_line
                , linewidth = 2
                , label = f'Ilumi ({ilumi_range[0]}-{round(ilumi_range[1],2)}): {round(coeffs[0], 5)}*x + {round(coeffs[1], 5)}')
    else:
        plt.plot( new_x
                , new_line
                , label = f'Entire range: {round(coeffs[0], 5)}*x + {round(coeffs[1], 5)}')
        
    plt.legend(loc='lower right')
    plt.legend()

    #plt.ylim(0.95, 1.05)

# ====================================================================
# Main script to extract corrections and generate plots
# for per fill data
# ====================================================================
# TODO: Include part to read David's csv with fill statistics
fi = pd.read_csv('input/year_data/history_run2.csv')
fi = fi[['fill_number', '2018 Lumi']].dropna()
fill_list_run2 = [int(x) for x in fi.fill_number.tolist()]
lumi_list_run2 = [float(x) for x in fi['2018 Lumi'].tolist()]

fi = pd.read_csv('input/year_data/history_23.csv')
fi = fi[['Fill', '2023 Lumi']].dropna()
fill_list23 = [int(x) for x in fi.Fill.tolist()]
lumi_list23 = [float(x) for x in fi['2023 Lumi'].tolist()]

fi = pd.read_csv('input/year_data/history_24.csv')
fi = fi[['Fill', '2024 Lumi']].dropna()
fill_list24 = [int(x) for x in fi.Fill.tolist()]
lumi_list24 = [float(x) for x in fi['2024 Lumi'].tolist()]

fill_list = np.concatenate((fill_list_run2, fill_list23, fill_list24))
lumi_list = np.concatenate((lumi_list_run2, lumi_list23, lumi_list24))

gInlumiFake = {}
for x in range(len(fill_list)):
    gInlumiFake[fill_list[x]] = lumi_list[x]

# Configure detector
parser = argparse.ArgumentParser(description="The tool is designed to analyse CMS BRIL data and generate DPG relevant plots and comparisons")

parser.add_argument('detector1', type=str, help='First detector in comparison')
parser.add_argument('detector2', type=str, help='Second detector in comparison')
parser.add_argument('config', type=str, help='Configuration file with detector meta data')

args = parser.parse_args()

config_file = args.config

with open(config_file, 'r') as file:
    meta_data = yaml.safe_load(file)

first_d = meta_data[args.detector1]
second_d = meta_data[args.detector2]

default_path = '/localdata/alshevel/hf_studies'
file_path = f"{default_path}/ratios_hd5/{first_d['node']}_{second_d['node']}"
file_path_sbil = f"{default_path}/sbil_ratios_hd5/{first_d['node']}_{second_d['node']}"

# Plotter part
df = hd5_reader.read_custom_data(file_path, 'ratio')
# Convert fill to ilumi

fill = []; ilumi = []; mean = []; std = []
for index, row in df.iterrows():
    try:
        ilumi.append(gInlumiFake[row['fill']])
        fill.append(row['fill'])
        mean.append(row['ratio'])
        std.append(row['error'])
    except:
        continue
    
df = pd.DataFrame({'fill': fill, 'ilumi': ilumi, 'ratio': mean, 'error': std}).dropna()
df = df[df.fill > 9100]

print(df.error.to_list())
df = df[df.error < 0.025]
df.ratio = df.ratio/ 1.008


#df = df[df.fill.between(6540, 7494, inclusive=True)]

fig = plotter.create_figure( 'Integrated Luminosity [fb-1]'
                           , f"{first_d['node']}/{second_d['node']}"
                           , '2024 (13.6 TeV)' )
                    
plt.errorbar( df.ilumi
            , df.ratio
            , yerr = df.error
            , marker='.'
            , color = 'b'
            , ecolor = 'b'
            , linestyle=''
            , capsize = 2.0
            , alpha = 0.5
            )

# plt vdm bar. Adjust to calculate actual intecept

plt.axvline( x = gInlumiFake[9639]
           , color = 'r'
           , ls='--'
           , label = f'vdM Fill 9639')

'''
plt.axhline( y = 0.96
           , color = 'b'
           , ls='--'
           , label = f'current online')
'''           

plt.ylim(0.93, 1.07)

# Do the fit in the loop
#fits = [[50, 58], [59.5, 68.5], [70, 78], [79, 90], [91.5, 95]]
fits = [[0, max(df.ilumi)]]
for fit in fits:
    do_linear_fit(df, fit)

plt.savefig('ratio.png', dpi = 500)
plt.show()