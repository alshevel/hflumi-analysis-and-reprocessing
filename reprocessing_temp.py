import pandas
import numpy as np
import matplotlib.pyplot as plt
import glob, pathlib, os

# custom modules
from pybrildpg import hf_correction
from pybrildpg import hd5_reader

# ====================================================================
# Main template for HF reprocessing. TODO: Should be made configurable
# ====================================================================
input_path = '/brildata/18/' #/brildpg/rawdata/17/
output_path = '/localdata/alshevel/hf_origin/hfet/24'
node = 'hfetlumi'
sigvis = 3333.96

stable_fills = [int(x.replace(input_path, '')) for x in glob.iglob(f'{input_path}/*', recursive = True)]
stable_fills.sort()

print(stable_fills)

stable_fills = [9639]

list_of_failed = []
for fill in stable_fills:
    
    try:
        # Final destination where to save
        fill_path = f'{output_path}/{fill}/'
        if not os.path.isdir(fill_path):
            os.makedirs(fill_path)

        print(fill)

        # Read beam data
        beam = hd5_reader.read_beam_fill(input_path, fill, 'beam')

        beam_slice = beam[['fillnum', 'runnum', 'lsnum', 'nbnum', 'status', 'collidable']]
        beam_loc = beam.loc[(beam['status'].str.decode("utf-8") == 'STABLE BEAMS')]
        beam_loc = beam_loc.loc[(beam_loc['fillnum'] == fill)]
        activeBXMask = np.array(beam_loc['collidable'].iloc[0])
        number_of_active = beam_loc['ncollidable'].iloc[0]

        # Start with run reprocessing   
        for path in pathlib.Path(f'{input_path}{fill}/').glob('*.hd5'):

            df_run = hd5_reader.read_run(path, node)

            file_name = str(path).replace(f'{input_path}{fill}/', '')

            #df_run['sbil'] = np.array([np.multiply(hist, activeBXMask).sum()/number_of_active for hist in np.stack(df_run['bxraw'])]).tolist()
            #df_run = df_run.round({'sbil': 5})
            #df_run = df_run.sort_values(by=['sbil'])

            #hf_correction.revert_pedestal(df_run)
            #hf_correction.revert_afterglow(df_run, activeBXMask, 'input/afterglow/hfet18.txt')

            # Extra corrections
            type1 = [0, 0, 0, 0]
            order1 = [0, 0, 0, 0]

            #hf_correction.apply_afterglow(df_run, activeBXMask, type1, order1, path = 'input/afterglow/hfet23.txt')
            hf_correction.subtract_pedestal(df_run)

            new_bx = [hist for hist in np.stack(df_run['bxraw'])]
            new_avg = [np.multiply(hist, activeBXMask).sum() for hist in new_bx]

            df_run['bxraw'] = new_bx                        
            df_run['avgraw'] = new_avg
            df_run['bx'] = df_run['bxraw'] * 11245.6/sigvis # new calibrations applied to a raw rate
            df_run['avg'] = df_run['avgraw'] * 11245.6/sigvis

            print(df_run)

            hd5_reader.save_to_hd5(df_run, node, fill_path, file_name)
    
    except:
        list_of_failed.append(fill)

print(list_of_failed)