import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse, yaml

# custom modules
from pybrildpg import plotter
from pybrildpg import hd5_reader

# ====================================================================
# Main script to extract corrections and generate plots
# for per fill data
# ====================================================================
# Configure detector
parser = argparse.ArgumentParser(description="The tool is designed to analyse CMS BRIL data and generate DPG relevant plots and comparisons")

parser.add_argument('detector1', type=str, help='First detector in comparison')
parser.add_argument('detector2', type=str, help='Second detector in comparison')
parser.add_argument('config', type=str, help='Configuration file with detector meta data')

args = parser.parse_args()

config_file = args.config

with open(config_file, 'r') as file:
    meta_data = yaml.safe_load(file)

first_d = meta_data[args.detector1]
second_d = meta_data[args.detector2]

default_path = '/localdata/alshevel/hf_studies'
file_path = f"{default_path}/ratios_hd5/{first_d['node']}_{second_d['node']}"
file_path_sbil = f"{default_path}/sbil_ratios_hd5/{first_d['node']}_{second_d['node']}"

# Plotter part
df = hd5_reader.read_custom_data(file_path_sbil, 'sbil_ratio').dropna()
#df = df[df.fill > 9100]
df = df[df.fill.between(6540, 7494, inclusive=True)]

ratios = hd5_reader.read_custom_data(file_path, 'ratio').dropna()
ratios = ratios[['fill', 'ratio']]
ratios = ratios.rename(columns={"ratio": "fill_ratio"})

df = pd.merge( df
             , ratios
             , on = ['fill'])

df['ratio'] = df['ratio']/df['fill_ratio']
df['ratio'] = 1/df['ratio']
print(df)

# Scatter style plot
fig = plotter.create_figure( 'SBIL [Hz/ub]'
                        , f'{first_d["alias"]}/{second_d["alias"]} Ratio'
                        , '2018 (13 TeV)'
                        )

x = df.sbil
y = df.ratio
H, xedges, yedges = np.histogram2d(x,y,bins=1000)
H = np.rot90(H)
H = np.flipud(H)
Hmasked = np.ma.masked_where(H==0,H)

plt.pcolormesh(xedges,yedges,Hmasked)
cbar = plt.colorbar()
cbar.ax.set_ylabel('Counts')

new_x = np.linspace(min(df.sbil), max(df.sbil), num=np.size(df.sbil))
coeffs = np.polyfit(df.sbil, df.ratio, 1)
new_line = np.polyval(coeffs, new_x)

plt.plot( new_x
        , new_line
        , color = 'r'
        , label = f'Linear fit')

plt.plot([], [], '.', label = f'Fit Param = {coeffs}')
plt.legend(loc='lower right')

plt.xlim(2, 10)
plt.ylim(0.925, 1.075)

plt.savefig(f'sbil_ratio_{first_d["alias"]}_{second_d["alias"]}.png', dpi = 500)

plt.show()